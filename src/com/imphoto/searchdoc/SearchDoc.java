package com.imphoto.searchdoc;

import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class SearchDoc {
    static Thread thread = new Thread();
    static Screen screen = new Screen();
    static int activePlayer = 1;
    static int i1, i2;
    static int score1, score2;
    static boolean gameEnd = true;
    private static final int ROUND_LENGTH = 3000;

    public static void main(String args[]) throws InterruptedException {
        // Initialize the screen form:
        JFrame frame = new JFrame("Search Doc Timer");
        frame.setContentPane(screen.getPnlScreen());
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setFocusable(true);

        frame.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                System.out.println(e.getKeyText(e.getKeyCode()));
                switch (e.getKeyCode()){
                    case KeyEvent.VK_ENTER:
                        if (gameEnd) {
                            resetGame();
                            gameEnd = false;
                        }
                    break;
                    case KeyEvent.VK_SPACE:
                        changePlayer();
                    break;
                    case KeyEvent.VK_ESCAPE:
                        if (gameEnd) {
                            resetGame();
                        }
                    break;
                }

            }
        });

        resetGame();
        playGame();
    }

    /**
     *
     * @throws InterruptedException
     */
    private static void playGame() throws InterruptedException {
        do {
            thread.sleep(10);
            if (!gameEnd) {
                gameLoop();
            }
        } while (true);
    }

    private static void gameLoop() {
        if (activePlayer == 1) {
            i1--;
        } else {
            i2--;
        }

        screen.setTextPlayer1Timer(i1);
        screen.setTextPlayer2Timer(i2);
        screen.setLblPlayer1(score1);
        screen.setLblPlayer2(score2);
        screen.setActivePlayer(activePlayer);

        if (i1 == 0 || i2 == 0) {
            gameEnd = true;
        } else {
            gameEnd = false;
        }
    }

    private static void changePlayer() {
        if (activePlayer == 1) {
            score1++;
            activePlayer = 2;
        } else {
            score2++;
            activePlayer = 1;
        }
    }

    private static void resetGame() {
        activePlayer = 1;
        i1 = ROUND_LENGTH;
        i2 = ROUND_LENGTH;
        score1 = score2 = 0;

        screen.setTextPlayer1Timer(i1);
        screen.setTextPlayer2Timer(i2);
        screen.setLblPlayer1(score1);
        screen.setLblPlayer2(score2);
        screen.setActivePlayer(activePlayer);
    }
}
