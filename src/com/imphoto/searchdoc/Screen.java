package com.imphoto.searchdoc;

import javax.swing.*;
import java.awt.*;

public class Screen {
    private JTextField textPlayer1Timer;

    public void Screen() {

    }

    public void setTextPlayer1Timer(int playerTimer) {
        if (playerTimer == 0) {
            this.textPlayer1Timer.setForeground(Color.RED);
        } else {
            this.textPlayer1Timer.setForeground(Color.BLACK);
        }
        this.textPlayer1Timer.setText(this.intToTime(playerTimer));
    }

    public void setTextPlayer2Timer(int playerTimer) {
        if (playerTimer == 0) {
            this.textPlayer2Timer.setForeground(Color.RED);
        } else {
            this.textPlayer2Timer.setForeground(Color.BLACK);
        }
        this.textPlayer2Timer.setText(this.intToTime(playerTimer));
    }

    private JTextField textPlayer2Timer;

    public JPanel getPnlScreen() {
        return pnlScreen;
    }

    private JPanel pnlScreen;

    public void setLblPlayer1(int score) {
        this.lblPlayer1.setText("Player 1 - Score: " + String.valueOf(score));
    }

    public void setLblPlayer2(int score) {
        this.lblPlayer2.setText("Player 1 - Score: " + String.valueOf(score));
    }

    private JLabel lblPlayer1;
    private JLabel lblPlayer2;

    private String intToTime(int playerTime) {
        int seconds = playerTime / 100;
        int millisec = playerTime % 100;

        return String.valueOf(String.format("%02d", seconds) + ':' + String.format("%02d", millisec));
    }

    public void setActivePlayer(int player) {
        if (player == 1) {
            lblPlayer1.setForeground(Color.GREEN);
            lblPlayer2.setForeground(Color.BLACK);
        } else {
            lblPlayer2.setForeground(Color.GREEN);
            lblPlayer1.setForeground(Color.BLACK);
        }
    }
}
